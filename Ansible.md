theme: sudodoki/reveal-cleaver-theme

--
## A mon sujet

* @katmlslg sur Twitter
* @katyucha@social.katyucha.ovh sur le Fédivers
* https://katyucha.ovh
* Sysadmin/Dev depuis 15+
* NixOS Addict

--
# Ansible
#### L'ami du sysadmin

```
{yum, apt, pip} install ansible
```

--
## C'était pas mieux avant

Celui qui vous dit le contraire vous ment.

--
## Principes de base

* Même effort pour 1 serveur, comme pour 1000
* Idempotence
* Agentless
  * (en fait.. y a un agent : c'est le serveur ssh)

--
## Derrière le rideau

* Python
* Format YAML

--
## Règles d'écriture

Yaml :
* 2 espaces
* variables : "{{ maVar }}"

--
## C'est très lisible

``` yaml
- name: create some stuff files
  file:
    state: touch
    path: "/tmp/{{ item }}"
  loop:
    - "toto"
    - "titi"
    - "tata"
```
(Nommez chaque action ... par pitié : la doc à minima)

--
## Préparation de notre environnement

* un coup de vagrant
  * https://framagit.org/Katyucha/ansible-2022.git
  * copiez votre clé publique dans ./cle_publique
  * cd 1
  * ``` vagrant up ```

--
## root or not ?
Creez ansible.cfg

```
[defaults]
remote_user=vagrant

[privilege_escalation]
become = true
```

--
## le fichier d'inventaire
./inventory
```
all:
  hosts:
    machine1:
      ansible_host: 192.168.33.15
```
--
## Ping ?!
```

* Premier ping

ansible -m ping -i inventory all
```
--
## Notre premier playbook

--
## Application des roles

Un ou plusieurs roles sur un ou plusieurs hosts

--
## Structure d'un playbook

* ensemble de roles
* chaque role est composé de
  * tasks : les actions
  * defaults : variables par défaut
  * vars : variables à modifier
  * files : à éviter
  * templates
  * handlers : actions
  * meta : informations et dépendances
  * d'un README !
* fichier principal: main.yml
--
## Faut bien comprendre la structure

--
## Au niveau répertoire ?

```
mkdir -p roles/mon-role
cd roles/mon-role
mkdir -p {tasks,templates,vars,defaults,files}
touch README.md
cd - #Astuce shell "echo $OLDPWD"
```

--
## Au boulot !

* Installons des packages : git, curl

--
## début

./monPlaybook.yml
```
#!/usr/bin/env ansible-playbook
- hosts: all
  roles:
    - role: mon-role
```

--

Dans roles/my-role/tasks, créons un fichier main.yml

```yaml
- name: Install a list of packages
  apt:
    name: "{{ item }}"
  loop:
    - git
    - curl
```
--
Et on lance :
```
ansible-playbook -i inventory monPlaybook.yml
```

--
Quelqu'un a vu le Gathering facts ?

--
On le regarde ?
```
ansible -m setup -i inventory all
```

--
## Fouillons dans la doc

https://docs.ansible.com/ansible/latest/index.html

--
#### Lecture Verticale

Pas bien !!
```
- name: copy configuration file
  template:
    src=keepalived.conf.j2 dest=/etc/keepalived/keepalived.conf
```

Bien !
```
- name: copy configuration file
  template:
    src: keepalived.conf.j2
    dest: /etc/keepalived/keepalived.conf

```
--
## Au boulot, encore !

* Créons un utilisateur user1

--
## Soluce

```
- name: add User1
  user:
    name: "user1"
    shell: "/bin/bash"
```
--
et on rajoute une clé pour se connecter:
```
- name: Set authorized key taken from file
  authorized_key:
    user: user1
    state: present
    key: "{{ lookup('file', 'xxxxx') }}"
```

--
## Conditions

Chaque action peut être conditionnée avec "when"

```
- name: Installation
  yum:
    name: nmon
  when: ansible_distribution == 'CentOS' or ansible_distribution == 'Red Hat Enterprise Linux'
```

--
## Conditions 2/2

ou dans les roles !
```
- hosts: all
  roles:
    - { role: common, when: myVar is defined }
```

--
## Au boulot

répertoire 2 du dépot : On démarre les 2 vms

rajout dans l'inventaire :
* L'une dans un groupe loadbalancer
* L'autre dans un groupe web

On modifie le playbook et on crée un role pour chacun.

Installation de nginx pour le groupe loadbalancer  
Installation de apache pour le groupe web

--
## Les fichiers Jinja2

* Système de template
* nommez les en .j2
* évidemment dans le répertoire "templates"
* Ne faites pas de COPY. Utilisez toujours template

--
## Exemple
```
upstream nginx-{{nomSite}} {
  {% for hosts in groups['web'] %}
  server {{hosts}}:80;
  {% endfor %}
}

# RP
server {
  listen 9000 ;
  server_name {{nomSite}};

  location / {
    proxy_pass http://nginx-{{nomSite}};
  }
}

```

--
## Avec des boucles

```
discovery.zen.ping.unicast.hosts: [  {% for hosts in groups['elastic'] %}
"{{hosts}}" {% if not loop.last %},{% endif %}
{% endfor %} ]
```
donne :

```
discovery.zen.ping.unicast.hosts: elastic1, elastic2, elastic3, elastic4
```

--
## Au boulot ! Encore et Encore

Automatiser des fichiers dans apache  
Trois variables :  répertoire, port et nom du site  
Spoil : regarder defaults/main.yml

```
<VirtualHost *:81>
        ServerAdmin webmaster@localhost
        DocumentRoot /var/www/site1

        ErrorLog ${APACHE_LOG_DIR}/error.log
        CustomLog ${APACHE_LOG_DIR}/access.log combined
</VirtualHost>
```

--
## Soluce
```
<VirtualHost *:{{ item.port }}>
        ServerAdmin webmaster@localhost
        DocumentRoot {{default_site_directory}}{{ item.nom_du_site }}

        ErrorLog ${APACHE_LOG_DIR}/error.log
        CustomLog ${APACHE_LOG_DIR}/access.log combined
</VirtualHost>
```

Dans les taches
```
- name: deploy apache site files
  template:
    src: "site.j2"
    dest: "/etc/apache2/sites-available/{{item.nom_du_site}}.conf"
  loop: "{{ sites }}"
```

--
## Regardons nginx

Lancer le role nginx sur le serveur loadbalancer.  
Comment corriger ?

Allons sur : http://192.168.33.18:9000

--
## Ansible Galaxy

* dépot de roles
* Bien lire le CODE
* Principe de base : Ne jamais faire confiance !

--
## TP

- Lundi 21 : un exo par personne par mail avec un depot
- A rendre 21 mars, 23:59
