# Cours Ansible 2022

## Generate html

```
npm install cleaver
cleaver Ansible.md
```


## Setup

Install the following:

- VirtualBox ([download](https://www.virtualbox.org/))
- Vagrant ([download](http://www.vagrantup.com/downloads.html))
- Ansible
  - `pip install ansible` via [pip](http://pip.readthedocs.org/en/latest/installing.html) (All Platforms)
  - `brew install ansible` via [homebrew](http://brew.sh/) (OSX)
  - `apt-get/yum install ansible` (Linux)

- copy your public_key in a "./cle_publique"
- go to 1 or 2 directory
- run : vagrant up
